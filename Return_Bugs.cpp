﻿#include <iostream>
#include <fstream>
#include <regex>
#include <string>
#include <map>
#include <vector>
#include <boost/filesystem/operations.hpp>

    void recording_files_name(const std::string str_directory, std::vector <std::string>& file_names)
    {
        boost::filesystem::directory_iterator begin(str_directory);
        boost::filesystem::directory_iterator end;

        for (; begin != end; ++begin)
        {
            boost::filesystem::file_status fs =
                boost::filesystem::status(*begin);

            if (begin->path().extension() == ".log")
                file_names.push_back(begin->path().string());
        }
    }

    void count_bugs(std::ifstream* file, std::map <std::string, int>& map_bugs)
    {
        std::string check_str;
        std::cmatch kind_bug;

        std::regex reg_bugs("TRACE\|INFO\|DEBUG\|WARN\|ERROR");

        while (*file)
        {
            getline(*file, check_str);
            if (regex_search(check_str.c_str(), kind_bug, reg_bugs))
                map_bugs[kind_bug[0]]++;
        }
    }

    void all_bugs_count(const std::vector <std::string>& file_names, std::map <std::string, int>& bugs)
    {
        std::ifstream file;
        for (int i = 0; i < file_names.size(); i++)
        {
            file.open(file_names[i]);
            if (!file.is_open())
            {
                std::cout << "File " + file_names[i] + " is not open" << "\n";
                continue;
            }

            count_bugs(&file, bugs);
            file.close();
        }
    }

    void output_bugs(std::map <std::string, int>& bugs)
    {

        for (auto& begin_it : bugs)
        {
            std::cout << begin_it.first << ": " << begin_it.second << "\n";
        }
    }

    void Logs(const std::string& directory)
    {

        std::map <std::string, int> bugs = { {"TRACE", 0}, {"INFO", 0}, {"DEBUG", 0}, {"WARN", 0}, {"ERROR", 0} };
        std::vector <std::string> file_names;

        recording_files_name(directory, file_names);
        all_bugs_count(file_names, bugs);

        output_bugs(bugs);
    }



void main()
{
    std::string directory = "C:\\Users\\Konstantin\\Desktop\\Logs";
    Logs(directory);
}

